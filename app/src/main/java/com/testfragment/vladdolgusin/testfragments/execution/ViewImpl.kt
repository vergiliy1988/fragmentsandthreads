package com.testfragment.vladdolgusin.testfragments.execution

interface ViewImpl {
    fun updateView(value: String, type: Int)
}