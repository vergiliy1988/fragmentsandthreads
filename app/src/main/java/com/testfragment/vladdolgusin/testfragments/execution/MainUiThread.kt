package com.testfragment.vladdolgusin.testfragments.execution

import android.os.Handler
import android.util.Log

class MainUiThread: ThreadMainImpl {


    private var view: ViewImpl? = null
    private var handler: Handler = Handler()

    companion object {

        private var mainUiThread: MainUiThread? = null

        fun getInstance(): MainUiThread? {

            if (mainUiThread == null) {
                mainUiThread = MainUiThread()
            }
            return mainUiThread
        }
    }

    fun sendMessage(obj: Any, threadName: String){
        val msg = handler.obtainMessage(0,0,0, obj)
        handler.sendMessage(msg)
        handler.post {
            Log.i("THREAD", threadName)
            if (view != null) {
                if (threadName == "FirstThread"){
                    view?.updateView(obj.toString(), 1)
                }
                if (threadName == "SecondThread") {
                    view?.updateView(obj.toString(), 2)
                }
            }
        }
    }

    override fun detachView() {
        mainUiThread?.view = null
    }
    override fun attachView(view: ViewImpl) {
        this.view = view
    }
}



