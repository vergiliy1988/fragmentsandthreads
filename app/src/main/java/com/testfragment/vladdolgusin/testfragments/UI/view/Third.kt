package com.testfragment.vladdolgusin.testfragments.UI.view

import android.content.Context
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v4.widget.NestedScrollView
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView

import com.testfragment.vladdolgusin.testfragments.R

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [Third.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [Third.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class Third : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_third, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        var rootV = view!!.findViewById<LinearLayout>(R.id.root)
        var scrollV = view!!.findViewById<ScrollView>(R.id.scrollV)

        for (i: Int in 1..10) {
            val textView = TextView(context)
            textView.gravity = Gravity.CENTER
            textView.text = "Позиция $i"
            textView.width = LinearLayout.LayoutParams.MATCH_PARENT
            textView.height = 100
            rootV.addView(textView)
        }

        scrollV.viewTreeObserver.addOnScrollChangedListener {

            //Log.i("Scroll", "x - ${scrollV.scrollX}, y - ${scrollV.scrollY}")
            for (item in 0 until rootV.childCount - 1){
                Log.i("Y POSITION", " position - ${scrollV.scrollY + 90}, Y - ${rootV.getChildAt(1).y}")
                if ((scrollV.scrollY + 90) >  rootV.getChildAt(item).y) {
                    val lp = rootV.getChildAt(item).layoutParams
                    lp.height = 300
                    rootV.getChildAt(item).layoutParams = lp
                }else{
                    val lp = rootV.getChildAt(item).layoutParams
                    lp.height = 100
                    rootV.getChildAt(item).layoutParams = lp
                }
            }
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

}
