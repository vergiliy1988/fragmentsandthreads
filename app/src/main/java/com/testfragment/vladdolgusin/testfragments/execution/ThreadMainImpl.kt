package com.testfragment.vladdolgusin.testfragments.execution

interface ThreadMainImpl {
    fun attachView(view: ViewImpl)
    fun detachView()
}