package com.testfragment.vladdolgusin.testfragments.UI.adapters

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.testfragment.vladdolgusin.testfragments.R
import com.testfragment.vladdolgusin.testfragments.model.User
import kotlinx.android.synthetic.main.user_item.view.*

class UserAdapter : RecyclerView.Adapter<UserAdapter.UserHolder>() {

    private var users: List<User> = ArrayList()
    private var firstVisibleElement = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserHolder {
        Log.i("Adapter", "onCreateViewHolder")
        return UserHolder(
                LayoutInflater.from(parent.context)
                        .inflate(R.layout.user_item, parent, false)
        )
    }

    override fun onBindViewHolder(viewHolder: UserHolder, position: Int) {
        viewHolder.bind(users[position])
        if (position == firstVisibleElement){
            val lp = viewHolder.itemView.layoutParams
            //lp.height = 300
           // viewHolder.itemView.layoutParams = lp
        }else{
            val lp = viewHolder.itemView.layoutParams
            //lp.height = LinearLayout.LayoutParams.WRAP_CONTENT
           // viewHolder.itemView.layoutParams = lp
        }
        Log.i("Adapter", "onBindViewHolder")
    }

    override fun getItemCount() = users.size

    fun refreshUsers(users: List<User>) {
        this.users = users
        notifyDataSetChanged()
    }

    fun checkScroll(position: Int) {
        firstVisibleElement = position
        //notifyItemChanged(position)
    }

    class UserHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(user: User) = with(itemView) {
            userName.text = user.name
            userDescription.text = user.description
        }
    }
}