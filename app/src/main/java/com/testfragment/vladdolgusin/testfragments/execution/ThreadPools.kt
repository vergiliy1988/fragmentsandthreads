package com.testfragment.vladdolgusin.testfragments.execution

import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

class ThreadPools {
    private val CORE_POOL_SIZE = 3
    private val MAX_POOL_SIZE = 5
    private val KEEP_ALIVE_TIME = 120L
    private val TIME_UNIT = TimeUnit.SECONDS
    private val WORK_QUEUE = LinkedBlockingQueue<Runnable>()
    private var threadPoolExecutor: ThreadPoolExecutor

    init {
        threadPoolExecutor = ThreadPoolExecutor(
                CORE_POOL_SIZE,
                MAX_POOL_SIZE,
                KEEP_ALIVE_TIME,
                TIME_UNIT,
                WORK_QUEUE)
    }

    companion object {
        private var threadExecutor: ThreadPools? = null
        fun getInstance(): ThreadPools? {
            if (threadExecutor == null) {
                threadExecutor = ThreadPools()
            }

            return threadExecutor
        }
    }

    fun execute(runnable: Runnable) {
        threadPoolExecutor.submit(runnable)
    }
}