package com.testfragment.vladdolgusin.testfragments.UI.view

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearSnapHelper
import android.support.v7.widget.OrientationHelper
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.testfragment.vladdolgusin.testfragments.R
import com.testfragment.vladdolgusin.testfragments.UI.adapters.UserAdapter
import com.testfragment.vladdolgusin.testfragments.model.UserData


class FirstFragment : Fragment() {

    private var listener: OnFragmentInteractionListener? = null
    private lateinit var userList: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.first_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val showSecondFragment =  view!!.findViewById(R.id.toSecondFragment) as Button
        userList = view!!.findViewById(R.id.userList) as RecyclerView

        showSecondFragment.setOnClickListener {
            (activity as MainActivity).addFragment(2)
        }

        val adapter = UserAdapter()
        userList.layoutManager = LinearLayoutManager(activity)
        userList.adapter = adapter

        userList.addOnScrollListener(object: RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                var viewItem = userList.findChildViewUnder(dx.toFloat(), dy.toFloat())

//                if (viewItem != null) {
//                    adapter.checkScroll(userList.getChildAdapterPosition(viewItem))
//                }
            }

        })

        val snapHelper = LinearSnapHelper() // Or PagerSnapHelper
        adapter.refreshUsers(UserData.getAnotherUsers())
        snapHelper.attachToRecyclerView(userList)
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

}
