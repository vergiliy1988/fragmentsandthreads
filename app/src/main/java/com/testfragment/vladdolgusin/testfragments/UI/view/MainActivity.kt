package com.testfragment.vladdolgusin.testfragments.UI.view

import android.animation.ObjectAnimator
import android.graphics.Color
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.widget.LinearLayout
import com.testfragment.vladdolgusin.testfragments.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), FirstFragment.OnFragmentInteractionListener, SecondFragment.OnFragmentInteractionListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var layout = LinearLayout(this)


        val wight: Float = 300f

        layout.layoutParams = LinearLayout.LayoutParams(wight.toInt(),
                LinearLayout.LayoutParams.MATCH_PARENT)

        layout.translationX = 0f
        layout.setBackgroundColor(Color.RED)

        //rootLayout.addView(layout)

        showFromLeft.setOnClickListener {
            val animation = ObjectAnimator.ofFloat(leftFrame, "translationX", 0f).apply {
                start()
            }
        }

        showFirstFragment.setOnClickListener{
            addFragment(1)
        }

        hideLeftFrame.setOnClickListener{

            var dpMetric = 300 * (this.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
            val animation = ObjectAnimator.ofFloat(leftFrame, "translationX", -dpMetric).apply {
                start()
            }
        }

        Log.i("FRAGMENT", "activity onCreate")
    }

    override fun onResume(){
        super.onResume()
        Log.i("FRAGMENT", "activity onResume")
    }

    override fun onStart(){
        super.onStart()
        Log.i("FRAGMENT", "activity onStart")
    }

    fun addFragment(type: Int){
        val transaction = supportFragmentManager.beginTransaction()
        when (type) {
            1 -> {
                val firstFragment = FirstFragment()
                transaction.replace(R.id.frameToFragment, firstFragment)
            }
            2 -> {
                val secondFragment = SecondFragment()
                transaction.replace(R.id.frameToFragment, secondFragment)
            }
            3 -> {
                val secondFragment = Third()
                transaction.replace(R.id.frameToFragment, secondFragment)
            }
        }
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }

    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

