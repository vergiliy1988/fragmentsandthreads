package com.testfragment.vladdolgusin.testfragments.model

data class User (
    var name: String = "",
    var description: String = ""
)