package com.testfragment.vladdolgusin.testfragments.UI.view

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.testfragment.vladdolgusin.testfragments.R
import com.testfragment.vladdolgusin.testfragments.execution.ViewImpl
import com.testfragment.vladdolgusin.testfragments.execution.MainUiThread
import com.testfragment.vladdolgusin.testfragments.execution.ThreadPools
import kotlinx.android.synthetic.main.second_fragment.*


class SecondFragment : Fragment(), ViewImpl {

    private var listener: OnFragmentInteractionListener? = null
    private var mMainUiThread: MainUiThread? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        var view = inflater.inflate(R.layout.second_fragment, container, false)
        val startThread =  view.findViewById(R.id.startThread) as Button
        val toThird =  view.findViewById(R.id.toThird) as Button

        var threadLoop = ThreadPools.getInstance()

        mMainUiThread = MainUiThread.getInstance()
        mMainUiThread?.attachView(this)

        startThread.setOnClickListener {
            threadLoop?.execute(FirstThread(mMainUiThread))
            threadLoop?.execute(SecondThread(mMainUiThread))
        }

        toThird.setOnClickListener {
            (activity as MainActivity).addFragment(3)
        }

        return view
    }

    override fun updateView(value: String, type: Int) {
        if(type == 1){
            valueFirstThread.text = value
        }else{
            valueSecondThread.text = value
        }

    }

    class FirstThread(private var mHandler: MainUiThread?): Runnable{
        override fun run() {
            var tmp = 1
            while (tmp < 10) {
                Log.i("THREAD 1"," $tmp")
                Thread.sleep(2000)
                mHandler?.sendMessage(tmp, this.javaClass.simpleName)
                tmp++
            }
        }
    }

    class SecondThread(private var mHandler: MainUiThread?): Runnable{
        override fun run() {
            var tmp = 10
            while (tmp < 20) {
                Log.i("THREAD 2"," $tmp")
                Thread.sleep(3000)
                mHandler?.sendMessage(tmp, this.javaClass.simpleName)
                tmp++
            }
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null

    }

    override fun onPause() {
        super.onPause()
        mMainUiThread?.detachView()
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

}
